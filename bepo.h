namespace Bepo {    
    #define Key_BP_Dollar Key_Backtick
    #define Key_BP_GuillemetDouble Key_1  
    #define Key_BP_GuillemetOuvrant Key_2
    #define Key_BP_GuillemetFermant Key_3  
    #define Key_BP_ParentheseOuvrante Key_4
    #define Key_BP_ParentheseFermante Key_5  
    #define Key_BP_Arobase Key_6
    #define Key_BP_Plus Key_7  
    #define Key_BP_TraitDUnion Key_8
    #define Key_BP_Slash Key_9  
    #define Key_BP_Asterisque Key_0
    #define Key_BP_B Key_Q  
    #define Key_BP_EAigu Key_W
    #define Key_BP_P Key_E  
    #define Key_BP_O Key_R
    #define Key_BP_EGrave Key_T  
    #define Key_BP_Circonflexe Key_Y
    #define Key_BP_V Key_U  
    #define Key_BP_D Key_I
    #define Key_BP_L Key_O  
    #define Key_BP_J Key_P
    #define Key_BP_Z Key_LeftBracket 
    #define Key_BP_A Key_A  
    #define Key_BP_U Key_S
    #define Key_BP_I Key_D  
    #define Key_BP_E Key_F
    #define Key_BP_Virgule Key_G  
    #define Key_BP_C Key_H
    #define Key_BP_T Key_J  
    #define Key_BP_S Key_K
    #define Key_BP_R Key_L
    #define Key_BP_N Key_Semicolon
    #define Key_BP_M Key_Quote 
    #define Key_BP_AGrave Key_Z
    #define Key_BP_Y Key_X  
    #define Key_BP_X Key_C
    #define Key_BP_Point Key_V  
    #define Key_BP_K Key_B
    #define Key_BP_Apostrophe Key_N  
    #define Key_BP_Q Key_M
    #define Key_BP_G Key_Comma  
    #define Key_BP_H Key_Period  
    #define Key_BP_F Key_Slash  
    #define Key_BP_CCedille Key_Backslash 
    #define Key_BP_W Key_RightBracket  
    #define Key_BP_ECirconflexe Key_NonUsBackslashAndPipe  
    #define Key_BP_Pourcent Key_Equals  
    #define Key_BP_Egal Key_Minus

    #define LSFT_T(key) MT(LeftShift, key)
    #define RSFT_T(key) MT(RightShift, key)
    #define RALT_T(key) MT(RightAlt, key)

}
